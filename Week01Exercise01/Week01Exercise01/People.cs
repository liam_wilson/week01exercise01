﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week01Exercise01
{
    class People
    {
        public string Name { get; set; }
        public string BirthPlace { get; set; }
        public string FavFood { get; set; }
        public string FavDrink { get; set; }

        public People(string _name, string _place, string _favfood, string _favdrink)
        {
            Name = _name;
            BirthPlace = _place;
            FavFood = _favfood;
            FavDrink = _favdrink;
        }
    }
}
