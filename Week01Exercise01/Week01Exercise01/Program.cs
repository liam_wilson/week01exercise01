﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week01Exercise01
{

    class Program
    {
        static void Main (string[] args)
        {
            var p1 = new People("Liam", "invercargill", "beans and rice", "speights");
            var p2 = new People("ben", "kati", "beans and rice", "coca cola");
            var p3 = new People("logan", "whakatane", "ben special", "peach tea");

            PrintToScreen(p1);
            PrintToScreen(p2);
            PrintToScreen(p3);
        }

        public static void PrintToScreen(People person)
        {
            Console.WriteLine($"My Name is {person.Name} and i am born in {person.BirthPlace}");
            Console.WriteLine($"i love to eat {person.FavFood} and love to drink {person.FavDrink}\n");
        }

    }
}
    